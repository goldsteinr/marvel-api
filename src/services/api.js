import axios from 'axios';

const api = axios.create({
  baseURL: 'https://gateway.marvel.com:443/v1/public/'
});

api.interceptors.request.use(config => {
  config.params = {
  apikey: '5a08d0fa775695f71d3c3e0769e4dbc8',
    ...config.params,
  };
  return config;
});

export default api;
