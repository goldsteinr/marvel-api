import React, { useState, useEffect, useCallback } from "react";
import { useSelector } from "react-redux";
import { useParams, Link } from "react-router-dom";
import { selectResults } from "../characters/charactersSlice";
import api from "../../services/api";

import charStyles from "../characters/Characters.module.css";
import styles from "./Details.module.css";

const Details = () => {
  const characters = useSelector(selectResults);
  const params = useParams();
  const [character, setCharacter] = useState(null);
  const [canEdit, setCanEdit] = useState(false);
  const [characterName, setCharacterName] = useState('');
  const [characterDescription, setCharacterDescription] = useState('');


  const updateCharacter = useCallback(
    (char) => {
      if (localStorage.getItem(char.id)) {
        const savedCharacter = JSON.parse(localStorage.getItem(char.id));
        setCharacterName(savedCharacter.name);
        setCharacterDescription(savedCharacter.description)
      } else {
        setCharacterName(char.name);
        setCharacterDescription(char.description);
      }
    },
    [],
  )

  useEffect(() => {
    if (characters.length > 0) {
      const char = characters.filter(
        (char) => char.id === Number(params.id)
      )[0];
      updateCharacter(char);
      return setCharacter(char);
    }
    api
      .get(`/characters/${params.id}`)
      .then((response) => response.data)
      .then((res) => {
        setCharacter(res.data.results[0]);
        updateCharacter(res.data.results[0])
      });
  }, [characters, params.id, updateCharacter]);


  function handleSubmit (evt) {
    evt.preventDefault();
    const characterId = character.id;
    const formData = {
      name: characterName,
      description: characterDescription
    };
    localStorage.setItem(characterId, JSON.stringify(formData));
    setCanEdit(false);
  }

  function handleCharacterName (e) {
    setCharacterName(e.target.value);
  }

  function handleCharacterDescription (e) {
    setCharacterDescription(e.target.value);
  }

  if (!character) return <p>Loading..</p>;

  return (
    <>
      <Link to="/" className={styles.backButton}>
        <span>&#10229;</span> GO BACK
      </Link>
      <section className={styles.container}>
        <div className={styles.cardContainer}>
          <section className={charStyles.charactersCard}>
            <div className={styles.characterImageWrapper}>
              <img
                className={styles.characterImage}
                src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                alt={character.name}
              />
            </div>
            {canEdit ? (
              <form className={styles.characterForm}>
                <input
                  className={styles.characterInputs}
                  onChange={handleCharacterName}
                  value={characterName}
                  placeholder={characterName}/>
                <textarea
                  className={styles.characterInputs}
                  onChange={handleCharacterDescription}
                  value={characterDescription}
                  placeholder={
                    characterDescription || `Insert some description for ${characterName}`
                  }
                  rows="8"
                  />
                <section className={styles.characterFormFooter}>
                  <button onClick={handleSubmit} type="submit" className={charStyles.charactersLink}>
                    <span role="img" aria-label="floppy disk">💾</span>
                    {" "} Save
                  </button>
                  <button onClick={() => setCanEdit(false)} className={charStyles.charactersLink}>
                    <span role="img" aria-label="red cross">❌</span>
                    {" "} Cancel
                  </button>
                </section>
              </form>
            ) : (
              <>
                <h1 className={styles.characterName}>{characterName}</h1>
                <p className={styles.characterDescription}>
                  {characterDescription.length > 0  ? (
                    characterDescription
                  ) : (
                    <>
                      <span role="img" aria-label="dead face">
                        😵{" "}
                      </span>
                      <i>Description not found</i>
                    </>
                  )}
                </p>

                <button className={charStyles.charactersLink} onClick={() => setCanEdit(true)}>
                <span role="img" aria-label="edit">📝</span>
                {" "} Edit Character</button>
              </>
            )}
          </section>
        </div>
        <div className={styles.cardContainer}>
          <section className={charStyles.charactersCard}>
            <section className={styles.cardContent}>
              <h2 className={styles.characterName}>
                {character.name}'s Series
              </h2>
              {character.series.items.length > 0 ? (
                <ul className={styles.characterSeries}>
                  {character.series.items.map((serie) => {
                    return <li key={serie.resourceURI}>{serie.name}</li>;
                  })}
                </ul>
              ) : (
                <p className={styles.characterDescription}>
                  <span role="img" aria-label="dead face">
                    😵
                  </span>
                  <i> No series found</i>
                </p>
              )}
            </section>
          </section>
        </div>
      </section>
      <Link to="/" className={styles.backButton}>
        <span>&#10229;</span> GO BACK
      </Link>
    </>
  );
};

export default Details;
