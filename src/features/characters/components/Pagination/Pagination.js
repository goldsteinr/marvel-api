import React from "react";
import styles from "./Pagination.module.css";

export default function Pagination({
  changePaginationRange,
  rangePages,
  totalPages,
  changeCurrentPage,
  currentPage,
}) {
  return (
    <section className={styles.container}>
      {totalPages ? (
        <>
          <button
            onClick={(evt) => changePaginationRange(evt, "prev")}
            className={styles.paginationItem}
            disabled={rangePages[0] === 1}
          >
            &#8606;
          </button>
          <ul className={styles.paginationWrapper}>
            {[...Array(totalPages).keys()].map((el, i) => {
              const normalOrder = i + 1;
              if (normalOrder >= rangePages[0] && normalOrder <= rangePages[1])
                return (
                  <li
                    key={el + 1}
                    onClick={() => changeCurrentPage(Number(el + 1))}
                    className={`
                    ${styles.paginationItem} ${
                      normalOrder === currentPage ? styles.active : ""
                    }`}
                  >
                    {el + 1}
                  </li>
                );
              else return null;
            })}
          </ul>
          <button
            onClick={(evt) => changePaginationRange(evt, "next")}
            className={styles.paginationItem}
            disabled={
              rangePages[1] === totalPages || rangePages[1] > totalPages
            }
          >
            &#8608;
          </button>
        </>
      ) : (
        ""
      )}
    </section>
  );
}
