import React from 'react';
import styles from './styles.module.css';

const Searchbar = ({ handleType, searchTerm, placeholder }) => {
  return (
    <section className={styles.container}>
      <form>
        <label for="search" style={{display: 'none'}}>Search</label>
        <input id="search" value={searchTerm} type="text" onChange={handleType} placeholder={placeholder} required/>
      </form>
    </section>
  )
}

export default Searchbar;
