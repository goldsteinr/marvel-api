import React from "react";
import { Link } from "react-router-dom";
import styles from "../../Characters.module.css";

export default function CharactersList({
    isLoadingCards,
    isLoading,
    characters,
    checkSavedCharacter
  }) {
  return isLoading ? (
    <ul className={styles.charactersList}>
      {[...Array(20).keys()].map((el) => isLoadingCards(el))}
    </ul>
  ) : (
    <ul className={styles.charactersList}>
      {characters.map((char) => {
        return (
          <li key={char.id} className={styles.charactersCard}>
            <div className={styles.charactersImageWrapper}>
              <img
                className={styles.charactersImage}
                src={`${char.thumbnail.path}.${char.thumbnail.extension}`}
                alt={char.name}
              />
            </div>
            <section className={styles.charactersContent}>
              <h3>{checkSavedCharacter(char, "name")}</h3>
              <p className={styles.charactersDescription}>
                {checkSavedCharacter(char, "description") ? (
                  checkSavedCharacter(char, "description")
                ) : (
                  <i>Description not found</i>
                )}
              </p>
            </section>
            <footer className={styles.charactersCardFooter}>
              <Link className={styles.charactersLink} to={`/${char.id}`}>
                See More
              </Link>
            </footer>
          </li>
        );
      })}
    </ul>
  );
}
