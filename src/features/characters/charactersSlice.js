import { createSlice } from '@reduxjs/toolkit';

export const charactersSlice = createSlice({
  name: 'characters',
  initialState: {
    isLoading: true,
    results: [],
    count: 0,
    offset: 0,
    total: 0,
    totalPages: 0,
    rangePages: [1, 5],
    currentPage: 1,
    searchTerm: ''
  },
  reducers: {
    addResults: (state, action) => {
      const { payload } = action;
      state.results = payload.results;
      state.count = payload.count;
      state.offset = payload.offset;
      state.total = payload.total;
      state.totalPages = payload.total !== 0 ? Math.ceil(payload.total / 20) : 0;
      state.isLoading = false;
    },
    setSearchTerm: (state, action) => {
      const {payload} = action;
      state.searchTerm = payload;
    },
    setCurrentPage: (state, action) => {
      const { payload } = action;
      state.currentPage = payload;
    },
    setIsLoading: (state,action) => {
      const { payload } = action;
      state.isLoading = payload;
    },
    setRangePages: (state, action) => {
      const { payload } = action;
      state.rangePages = payload;
    }
  },
});

export const { setRangePages, addResults, setSearchTerm, setCurrentPage, setIsLoading } = charactersSlice.actions;

export const selectResults = state => state.characters.results;

export const selectTotalPages = state => state.characters.totalPages;

export const selectTotal = state => state.characters.total;

export const selectCount = state => state.characters.count;

export const selectSearchTerm = state => state.characters.searchTerm;

export const selectCurrentPage = state => state.characters.currentPage;

export const selectIsLoading = state => state.characters.isLoading;

export const selectRangePages = state => state.characters.rangePages;


export default charactersSlice.reducer;
