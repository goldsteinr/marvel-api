import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import styles from "./Characters.module.css";

import api from "../../services/api";
// import useDebounce from "../../hooks/useDebounce";
import {
  addResults,
  selectResults,
  selectSearchTerm,
  setSearchTerm,
  selectTotalPages,
  selectCurrentPage,
  setCurrentPage,
  setIsLoading,
  selectIsLoading,
  selectRangePages,
  setRangePages,
  selectTotal
} from "./charactersSlice";

import Searchbar from "./components/Searchbar/Searchbar";
import Pagination from "./components/Pagination/Pagination";
import CharactersList from "./components/CharactersList/CharactersList";

export default function Characters() {
  const characters = useSelector(selectResults);
  const isLoading = useSelector(selectIsLoading);
  const currentPage = useSelector(selectCurrentPage);
  const totalPages = useSelector(selectTotalPages);
  const searchTerm = useSelector(selectSearchTerm);
  const rangePages = useSelector(selectRangePages);
  const total = useSelector(selectTotal);
  const dispatch = useDispatch();

  const offset = (20 * (currentPage - 1)) > total ? total : 20 * (currentPage - 1);

  const buildRequestEndpoint = useCallback(() => {
    return `/characters?offset=${offset}${
      searchTerm?.length > 0 ? `&nameStartsWith=${searchTerm}` : ""
    }`;
  }, [offset, searchTerm]);

  const resquestCharacters = useCallback(async () => {
    dispatch(setIsLoading(true));
    return await api
      .get(buildRequestEndpoint())
      .then((response) => response.data)
      .then((res) => {
        const { data } = res;
        dispatch(addResults(data));
      });
  }, [dispatch, buildRequestEndpoint]);

  useEffect(() => {
    resquestCharacters();
  }, [resquestCharacters]);

  const changeCurrentPage = useCallback((page) => {
    return dispatch(setCurrentPage(page));
  }, [dispatch]);

  const changePaginationRange = useCallback((evt, type) => {
    evt.preventDefault();
    const [start, end] = rangePages;
    let newRangePages;
    if (type === "next") newRangePages = [Number(start) + 5, Number(end) + 5];
    else if (type === "prev")
      newRangePages = [Number(start) - 5, Number(end) - 5];

      else if (type === 'reset') newRangePages = [1, 5];

      if (newRangePages[0] < 1) newRangePages[0] = 1;
      if (newRangePages[1] >= totalPages && totalPages !== 0) newRangePages[1] = totalPages;
    dispatch(setRangePages(newRangePages));
    dispatch(setCurrentPage(newRangePages[0]))
  }, [rangePages, totalPages, dispatch]);

  const setSearch = useCallback((evt) => {
    const { value } = evt.target;
    dispatch(setCurrentPage(1));
    dispatch(setSearchTerm(value));
    changePaginationRange(evt, 'reset');
  }, [dispatch, changePaginationRange]);

  const clearSearch = (evt) => {
    evt.preventDefault();
    dispatch(setCurrentPage(1));
    dispatch(setSearchTerm(''));
    changePaginationRange(evt, 'reset');
  };

  const checkSavedCharacter = useCallback((character, key) => {
    if (localStorage.getItem(character.id)) {
      const savedCharacter = JSON.parse(localStorage.getItem(character.id));
      return savedCharacter[key];
    } else {
      return character[key];
    }
  }, []);

  const isLoadingCards = useCallback((i) => {
    return (
      <li key={i} className={styles.charactersCardLoading}></li>
    );
  }, []);

  return (
    <section>
      <Searchbar
        searchTerm={searchTerm}
        handleType={setSearch}
        placeholder="SEARCH CHARACTERS"
      />
      <Pagination
        rangePages={rangePages}
        changePaginationRange={changePaginationRange}
        currentPage={currentPage}
        changeCurrentPage={changeCurrentPage}
        totalPages={totalPages}
      />
      {!isLoading && characters.length === 0 && (
        <section className={styles.charactersListEmpty}>
          <p>
            <span role="img" aria-label="cat crying">😿</span>
            {" "} No character found!
          </p>
          <button onClick={clearSearch}>
            <span role="img" aria-label="monkey blind">❌</span>
            {" "} Clear Search
          </button>
        </section>
      )}
      <CharactersList
        isLoadingCards={isLoadingCards}
        characters={characters}
        isLoading={isLoading}
        checkSavedCharacter={checkSavedCharacter} />
      <Pagination
        rangePages={rangePages}
        currentPage={currentPage}
        changeCurrentPage={changeCurrentPage}
        changePaginationRange={changePaginationRange}
        totalPages={totalPages}
      />
    </section>
  );
}
