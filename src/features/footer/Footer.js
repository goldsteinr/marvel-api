import React from 'react'
import styles from './Footer.module.css'

export default function Footer() {
  return (
    <footer className={styles.container}>
      <div className={styles.footerSvgContainer}>
        <svg width="32px" height="52px" viewBox="0 0 36 52" xmlns="http://www.w3.org/2000/svg">
          <rect fill="#EC1D24" width="100%" height="100%"></rect>
          <path fill="#FEFEFE" d="M31.5 48V4H21.291l-3.64 22.735L14.102 4H4v44h8V26.792L15.577 48h4.229l3.568-21.208V48z"></path>
        </svg>
      </div>
      <nav
        aria-label="Legal footer navigtion"
      >
        <a
          className={styles.footerLink}
          target="_self"
          href="https://disneytermsofuse.com"
          >Terms of Use</a>
        <a
          className={styles.footerLink}
          target="_self"
          href="https://privacy.thewaltdisneycompany.com/en"
          >Privacy Policy</a>
        <a
          className={styles.footerLink}
          target="_self"
          href="https://www.marvel.com/corporate/license_tou"
          >License Agreement</a>
        <a
          className={styles.footerLink}
          target="_self"
          href="https://www.marvel.com/corporate/insider_terms"
          >Marvel Insider Terms</a>
        <span className={styles.footerText}>© 2020 MARVEL</span>
      </nav>
    </footer>
  )
}
