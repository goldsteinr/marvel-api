import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Characters from "./features/characters/Characters";
import Details from "./features/character-details/Details";

const Routes = () => {
  return (
    <Router>
      <Switch>
          <Route exact path="/">
            <Characters />
          </Route>
          <Route path="/:id">
            <Details />
          </Route>
        </Switch>
    </Router>
  )
}

export default Routes;
