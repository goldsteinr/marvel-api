import React from 'react';
import './App.css';
import Routes from './routes';
import Header from './features/header/Header';
import Footer from './features/footer/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes />
      <Footer/>
    </div>
  );
}

export default App;
