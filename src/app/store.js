import { configureStore } from '@reduxjs/toolkit';
import charactersReducer from '../features/characters/charactersSlice';

export default configureStore({
  reducer: {
    characters: charactersReducer,
  },
});
